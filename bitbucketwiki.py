#!/usr/bin/env python
# coding: utf-8
"""
Copyright (c) 2013 noonworks

Permission is hereby granted, free of charge, to any person obtaining 
a copy of this software and associated documentation files (the "Software"), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
"""

"""
replace `Title <Filename>`_ to `<Title Filename.html>`_
"""

import re

# Link pattern
_BITBUCKETWIKI_LINK_PAT = re.compile(ur"`(.+? )?<(.+?)>`_")
# Except pattern (normal hyperlink)
_BITBUCKETWIKI_EXCEPT_PATS = (re.compile(ur"://"), re.compile(ur"/$"), \
                              re.compile(ur"\.([^/]+)?$"))

def fix_doc_link_repl(matchobj):
  title = matchobj.group(1) if matchobj.group(1) else ur""
  path = matchobj.group(2)
  for pat in _BITBUCKETWIKI_EXCEPT_PATS:
    if pat.search(path):
      return ur"`" + title + "<" + path + ">`_"
  return ur"`" + title + "<" + path + ".html>`_"

def fix_doc_link(app, docname, source):
  source[0] = _BITBUCKETWIKI_LINK_PAT.sub(fix_doc_link_repl, source[0])

def setup(app):
  app.add_config_value('bitbucketwiki_fix_doc_link', True, True)
  app.connect("source-read", fix_doc_link)
